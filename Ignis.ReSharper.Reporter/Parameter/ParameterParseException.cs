﻿namespace Ignis.ReSharper.Reporter.Parameter;

public sealed class ParameterParseException(string message) : Exception(message);
