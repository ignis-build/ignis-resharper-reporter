﻿using Ignis.ReSharper.Reporter.Logging;
using Ignis.ReSharper.Reporter.Parameter;
using Directory = Ignis.ReSharper.Reporter.IO.Directory;

namespace Ignis.ReSharper.Reporter.Out;

internal sealed class FileOutput : Output, IEquatable<FileOutput>
{
    private readonly string _path;
    private readonly Lazy<Stream> _stream;

    public FileOutput(string path)
    {
        _path = path;
        _stream = new Lazy<Stream>(CreateOutputFile);
    }

    public bool Equals(FileOutput? other)
    {
        if (ReferenceEquals(null, other)) return false;
        if (ReferenceEquals(this, other)) return true;
        return _path == other._path;
    }

    public override void Dispose()
    {
        if (_stream.IsValueCreated) _stream.Value.Dispose();
    }

    public override Stream Stream()
    {
        return _stream.Value;
    }

    public override ExportParameters ToParameters()
    {
        return new ExportParameters()
            .Add("file", _path);
    }

    public override void Complete(Logger logger)
    {
        if (_stream.IsValueCreated) _stream.Value.Flush();

        logger.WriteLine($"Export to {_path}.");
    }

    public override bool Equals(object? obj)
    {
        return ReferenceEquals(this, obj) || (obj is FileOutput other && Equals(other));
    }

    public override int GetHashCode()
    {
        return _path.GetHashCode();
    }

    public static bool operator ==(FileOutput? left, FileOutput? right)
    {
        return Equals(left, right);
    }

    public static bool operator !=(FileOutput? left, FileOutput? right)
    {
        return !Equals(left, right);
    }

    public override string ToString()
    {
        return string.Join(";", ToParameters());
    }

    private FileStream CreateOutputFile()
    {
        Directory.FromFile(_path).Mkdirp();
        return System.IO.File.Create(_path);
    }

    public static Output? TryParse(ExportParameters parameters)
    {
        var file = parameters.TryValue("file");
        if (file != null) return new FileOutput(file);
        return null;
    }
}
