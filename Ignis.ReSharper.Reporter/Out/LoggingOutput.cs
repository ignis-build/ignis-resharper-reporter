﻿using Ignis.ReSharper.Reporter.Logging;
using Ignis.ReSharper.Reporter.Parameter;

namespace Ignis.ReSharper.Reporter.Out;

internal sealed class LoggingOutput : Output
{
    private readonly MemoryStream _stream = new();

    private LoggingOutput()
    {
    }

    public static Output Instance { get; } = new LoggingOutput();

    public override void Dispose()
    {
        _stream.Dispose();
    }

    public override Stream Stream()
    {
        return _stream;
    }

    public override ExportParameters ToParameters()
    {
        return ExportParameters.Empty;
    }

    public override void Complete(Logger logger)
    {
        logger.CopyFrom(_stream);
    }
}
