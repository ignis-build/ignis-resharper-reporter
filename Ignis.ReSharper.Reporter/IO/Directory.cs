﻿namespace Ignis.ReSharper.Reporter.IO;

internal sealed class Directory(string value, FileSystem fileSystem)
{
    public bool Exists => fileSystem.ExistsDirectory(value) || value is "." or "..";

    public void Mkdirp()
    {
        if (Exists) return;

        Parent().Mkdirp();
        fileSystem.Mkdir(value);
    }

    private Directory Parent()
    {
        var directory = GetDirectoryName(value);
        return new Directory(directory, fileSystem);
    }

    public static Directory FromFile(string file)
    {
        return FromFile(file, FileSystem.Default);
    }

    internal static Directory FromFile(string file, FileSystem fileSystem)
    {
        var directory = GetDirectoryName(file);
        return new Directory(directory, fileSystem);
    }

    public override string ToString()
    {
        return value;
    }

    private static string GetDirectoryName(string path)
    {
        var parent = Path.GetDirectoryName(path) ?? throw new InvalidOperationException();
        if (parent == string.Empty) return ".";
        return parent;
    }
}
