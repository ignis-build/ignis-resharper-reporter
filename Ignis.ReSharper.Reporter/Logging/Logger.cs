﻿namespace Ignis.ReSharper.Reporter.Logging;

public abstract class Logger
{
    public abstract void WriteLine(string message);

    public void CopyFrom(Stream stream)
    {
        var original = stream.Position;
        stream.Position = 0;

        try
        {
            using var reader = new StreamReader(stream, null!, true, -1, true);
            while (!reader.EndOfStream) WriteLine(reader.ReadLine()!);
        }
        finally
        {
            stream.Position = original;
        }
    }

    /// <summary>
    ///     Get logger. If <paramref name="logger" /> is null, returns <see cref="NullLogger" />.
    /// </summary>
    /// <param name="logger"></param>
    /// <returns></returns>
    public static Logger Get(Logger? logger)
    {
        return logger ?? NullLogger.Instance;
    }
}
