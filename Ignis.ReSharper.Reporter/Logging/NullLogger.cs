﻿namespace Ignis.ReSharper.Reporter.Logging;

internal sealed class NullLogger : Logger
{
    private NullLogger()
    {
    }

    public static Logger Instance { get; } = new NullLogger();

    public override void WriteLine(string message)
    {
        // do nothing.
    }
}
