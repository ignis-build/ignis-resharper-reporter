﻿using System.Collections.Immutable;

namespace Ignis.ReSharper.Reporter.Logging;

public sealed class AggregateLogging(IEnumerable<Logger> items) : Logger
{
    private readonly IEnumerable<Logger> _items = items.ToImmutableArray();

    public AggregateLogging(params Logger[] items) : this((IEnumerable<Logger>)items)
    {
    }

    public override void WriteLine(string message)
    {
        foreach (var logger in _items) logger.WriteLine(message);
    }
}
