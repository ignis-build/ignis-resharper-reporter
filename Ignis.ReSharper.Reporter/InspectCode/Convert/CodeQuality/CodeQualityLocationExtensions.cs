﻿using Ignis.ReSharper.Reporter.InspectCode.Reports.Elements;

namespace Ignis.ReSharper.Reporter.InspectCode.Convert.CodeQuality;

internal static class CodeQualityLocationExtensions
{
    public static CodeQualityLocation ToCodeQualityLocation(this Location location)
    {
        return new CodeQualityLocation(location);
    }
}
