﻿using System.Text.Json.Serialization;

namespace Ignis.ReSharper.Reporter.InspectCode.Convert.CodeQuality;

[JsonConverter(typeof(StringValueJsonConverter<FilePath>))]
public sealed class FilePath(string value) : IEquatable<FilePath>
{
    private readonly string _value = value.Replace("\\", "/");

    public bool Equals(FilePath? other)
    {
        if (ReferenceEquals(null, other)) return false;
        if (ReferenceEquals(this, other)) return true;
        return _value == other._value;
    }

    public override string ToString()
    {
        return _value;
    }

    public override bool Equals(object? obj)
    {
        return ReferenceEquals(this, obj) || (obj is FilePath other && Equals(other));
    }

    public override int GetHashCode()
    {
        return _value.GetHashCode();
    }

    public static bool operator ==(FilePath? left, FilePath? right)
    {
        return Equals(left, right);
    }

    public static bool operator !=(FilePath? left, FilePath? right)
    {
        return !Equals(left, right);
    }
}
