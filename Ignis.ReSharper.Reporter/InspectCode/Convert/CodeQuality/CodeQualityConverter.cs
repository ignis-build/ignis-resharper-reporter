﻿using System.Text.Json;
using Ignis.ReSharper.Reporter.InspectCode.Reports.Elements;

namespace Ignis.ReSharper.Reporter.InspectCode.Convert.CodeQuality;

[ReportConverter("codequality")]
public sealed class CodeQualityConverter : IReportConverter, IEquatable<CodeQualityConverter>
{
    private static readonly JsonSerializerOptions DefaultJsonOptions = new()
    {
        PropertyNamingPolicy = JsonNamingPolicy.CamelCase,
        WriteIndented = true
    };

    public bool Equals(CodeQualityConverter? other)
    {
        if (other == null) return false;
        return true;
    }

    public void Export(Report report, Stream stream)
    {
        var issues = report.ToCodeQualityIssues().ToArray();
        JsonSerializer.Serialize(stream, issues, DefaultJsonOptions);
    }

    public override bool Equals(object? obj)
    {
        return ReferenceEquals(this, obj) || (obj is CodeQualityConverter other && Equals(other));
    }

    public override int GetHashCode()
    {
        return 0;
    }

    public static bool operator ==(CodeQualityConverter? left, CodeQualityConverter? right)
    {
        return Equals(left, right);
    }

    public static bool operator !=(CodeQualityConverter? left, CodeQualityConverter? right)
    {
        return !Equals(left, right);
    }
}
