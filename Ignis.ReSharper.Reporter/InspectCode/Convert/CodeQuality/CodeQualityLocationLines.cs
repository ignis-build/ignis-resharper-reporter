﻿// ReSharper disable UnusedMember.Global

namespace Ignis.ReSharper.Reporter.InspectCode.Convert.CodeQuality;

internal sealed record CodeQualityLocationLines(int Begin)
{
    public int Begin { get; } = Begin;
}
