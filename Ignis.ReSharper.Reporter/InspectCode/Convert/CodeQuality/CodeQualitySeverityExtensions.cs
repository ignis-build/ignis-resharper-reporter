﻿using Ignis.ReSharper.Reporter.InspectCode.Reports.Elements;

namespace Ignis.ReSharper.Reporter.InspectCode.Convert.CodeQuality;

internal static class CodeQualitySeverityExtensions
{
    public static CodeQualitySeverity ToCodeQualitySeverity(this Severity severity)
    {
        return CodeQualitySeverity.Get(severity);
    }
}
