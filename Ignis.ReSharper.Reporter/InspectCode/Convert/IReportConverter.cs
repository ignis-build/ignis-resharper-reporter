﻿using Ignis.ReSharper.Reporter.InspectCode.Reports.Elements;

namespace Ignis.ReSharper.Reporter.InspectCode.Convert;

public interface IReportConverter
{
    void Export(Report report, Stream stream);
}
