﻿using Ignis.ReSharper.Reporter.InspectCode.Reports.Elements;
using Ignis.ReSharper.Reporter.InspectCode.Statistics;

// ReSharper disable MemberCanBePrivate.Global
// ReSharper disable UnusedAutoPropertyAccessor.Global

namespace Ignis.ReSharper.Reporter.InspectCode.Convert.Summary;

internal sealed class SeverityCount
{
    public SeverityCount(Severity severity, IssueStatistics statistics) :
        this(severity, statistics.Severities[severity])
    {
    }

    private SeverityCount(Severity severity, int count)
    {
        Severity = severity;
        Count = count;
    }

    public Severity Severity { get; }
    public int Count { get; }

    public static IEnumerable<SeverityCount> Rows(IssueStatistics statistics)
    {
        return Severity.All.Select(severity => new SeverityCount(severity, statistics));
    }
}
