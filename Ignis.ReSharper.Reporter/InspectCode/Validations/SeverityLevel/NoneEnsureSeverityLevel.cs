﻿using Ignis.ReSharper.Reporter.InspectCode.Reports.Elements;

namespace Ignis.ReSharper.Reporter.InspectCode.Validations.SeverityLevel;

internal sealed class NoneEnsureSeverityLevel : EnsureSeverityLevel
{
    private NoneEnsureSeverityLevel() : base("none")
    {
    }

    public static EnsureSeverityLevel Instance { get; } = new NoneEnsureSeverityLevel();

    public override void EnsureNoIssues(IssueCollection issues)
    {
        // no check issues.
    }
}
