﻿using System.Collections.Immutable;
using Ignis.ReSharper.Reporter.InspectCode.Reports.Elements;

namespace Ignis.ReSharper.Reporter.InspectCode.Validations.SeverityLevel;

internal sealed class SeverityEnsureSeverityLevel : EnsureSeverityLevel
{
    private readonly Severity _severity;

    public SeverityEnsureSeverityLevel(Severity severity, IEnumerable<Severity> higherOrEquals) :
        base(severity.ToString())
    {
        _severity = severity;
        higherOrEquals.ToImmutableArray();
    }

    public override void EnsureNoIssues(IssueCollection issues)
    {
        foreach (var severity in _severity.GetAllHigherOrEquals()) issues.Where(severity).EnsureNoIssues();
    }
}
