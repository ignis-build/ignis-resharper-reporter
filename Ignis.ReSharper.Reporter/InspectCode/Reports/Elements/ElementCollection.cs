﻿using System.Collections.Immutable;

namespace Ignis.ReSharper.Reporter.InspectCode.Reports.Elements;

public abstract class ElementCollection<T>(IEnumerable<T> items) : IEquatable<ElementCollection<T>>
{
    protected readonly ImmutableArray<T> Items = items.ToImmutableArray();

    public bool Equals(ElementCollection<T>? other)
    {
        if (ReferenceEquals(null, other)) return false;
        if (ReferenceEquals(this, other)) return true;
        return Items.SequenceEqual(other.Items);
    }

    public override bool Equals(object? obj)
    {
        if (ReferenceEquals(null, obj)) return false;
        if (ReferenceEquals(this, obj)) return true;
        if (obj.GetType() != GetType()) return false;
        return Equals((ElementCollection<T>)obj);
    }

    public override int GetHashCode()
    {
        return Items.GetHashCode();
    }

    public static bool operator ==(ElementCollection<T>? left, ElementCollection<T>? right)
    {
        return Equals(left, right);
    }

    public static bool operator !=(ElementCollection<T>? left, ElementCollection<T>? right)
    {
        return !Equals(left, right);
    }

    public override string ToString()
    {
        var values = string.Join(", ", Items);
        return $"[{values}]";
    }

    public IEnumerable<T> AsEnumerable()
    {
        return Items;
    }

    public int Count()
    {
        return Items.Length;
    }

    // ReSharper disable once MemberCanBeProtected.Global
    public bool IsEmpty()
    {
        return !Items.Any();
    }
}
