﻿using System.Collections.Immutable;

namespace Ignis.ReSharper.Reporter.InspectCode.Reports.Elements;

public sealed class Severity : IEquatable<Severity>, IComparable<Severity>
{
    private static readonly Lazy<IDictionary<string, Severity>> Map;

    private readonly string _id;
    private readonly int _order;

    static Severity()
    {
        Info = new Severity(nameof(Info), 1);
        Hint = new Severity(nameof(Hint), 2);
        Suggestion = new Severity(nameof(Suggestion), 3);
        Warning = new Severity(nameof(Warning), 4);
        Error = new Severity(nameof(Error), 5);
        All = new[]
        {
            Info,
            Hint,
            Suggestion,
            Warning,
            Error
        }.ToImmutableArray();

        Map = new Lazy<IDictionary<string, Severity>>(() => CreateMap(All));
    }

    private Severity(string id, int order)
    {
        _order = order;
        _id = id.ToUpperInvariant();
    }

    public static Severity Error { get; }
    public static Severity Warning { get; }
    public static Severity Suggestion { get; }
    public static Severity Hint { get; }
    public static Severity Info { get; }

    public static ImmutableArray<Severity> All { get; }

    public int CompareTo(Severity? other)
    {
        if (ReferenceEquals(this, other)) return 0;
        if (ReferenceEquals(null, other)) return 1;
        return _order.CompareTo(other._order);
    }

    public bool Equals(Severity? other)
    {
        if (ReferenceEquals(null, other)) return false;
        if (ReferenceEquals(this, other)) return true;
        return _id == other._id;
    }

    public IEnumerable<Severity> GetAllHigherOrEquals()
    {
        return All.Where(x => x.HigherOrEquals(this));
    }

    private static Dictionary<string, Severity> CreateMap(IEnumerable<Severity> severities)
    {
        return severities
            .ToDictionary(s => s._id, s => s);
    }

    public override string ToString()
    {
        return _id;
    }

    public static Severity Get(string? id)
    {
        if (id == null) throw new ArgumentNullException(nameof(id));
        return Map.Value[id];
    }

    public static Severity? TryGet(string? id)
    {
        if (id == null) return null;

        if (Map.Value.TryGetValue(id, out var value)) return value;

        return null;
    }

    public override bool Equals(object? obj)
    {
        return ReferenceEquals(this, obj) || (obj is Severity other && Equals(other));
    }

    public override int GetHashCode()
    {
        return _id.GetHashCode();
    }

    public static bool operator ==(Severity? left, Severity? right)
    {
        return Equals(left, right);
    }

    public static bool operator !=(Severity? left, Severity? right)
    {
        return !Equals(left, right);
    }

    public bool HigherOrEquals(Severity other)
    {
        return CompareTo(other) >= 0;
    }

    public static IEnumerable<Severity> GetHigherOrEquals(Severity? severity)
    {
        if (severity == null) return All;

        return All.Where(x => x.HigherOrEquals(severity));
    }
}
