﻿using System.Xml.Linq;

// ReSharper disable UnusedMember.Global

namespace Ignis.ReSharper.Reporter.InspectCode.Reports.Elements;

public sealed record Issue(string? Message, Location Location, IssueType Type, Severity? Severity = null)
{
    private Issue(string? message, Location location, string typeId, IssueTypeCollection issueTypes,
        Severity? severity) :
        this(message, location, issueTypes.Find(typeId), severity)
    {
    }

    public string? Message { get; } = Message;
    public Location Location { get; } = Location;
    public IssueType Type { get; } = Type;
    public Severity Severity { get; } = Severity ?? Type.Severity;

    public static Issue Get(XElement element, IssueTypeCollection issueTypes)
    {
        var typeId = element.Attribute("TypeId")?.Value ?? throw new InvalidOperationException();
        return new Issue(
            element.Attribute(nameof(Message))?.Value,
            Location.Get(element),
            typeId,
            issueTypes,
            TryParseSeverity(element));
    }

    public static IEnumerable<Issue> All(XElement? element, IssueTypeCollection issueTypes)
    {
        if (element == null) return Enumerable.Empty<Issue>();

        return element.Elements(nameof(Issue)).Select(e => Get(e, issueTypes));
    }

    public bool Is(Severity severity)
    {
        return Severity == severity;
    }

    private static Severity? TryParseSeverity(XElement element)
    {
        var severity = element.Attribute(nameof(Elements.Severity))?.Value;
        return Severity.TryGet(severity);
    }
}
