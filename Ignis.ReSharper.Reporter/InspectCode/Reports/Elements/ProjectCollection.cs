﻿using System.Xml.Linq;

namespace Ignis.ReSharper.Reporter.InspectCode.Reports.Elements;

public sealed class ProjectCollection(IEnumerable<Project> items) : ElementCollection<Project>(items)
{
    public static ProjectCollection Empty { get; } = new(Array.Empty<Project>());

    public static ProjectCollection Get(XElement? element, IssueTypeCollection issueTypes)
    {
        var issues = element?.Element("Issues");
        if (issues == null) return Empty;

        return new ProjectCollection(Project.All(issues, issueTypes));
    }

    public static implicit operator ProjectCollection(Project[] items)
    {
        return new ProjectCollection(items);
    }

    public IssueCollection All()
    {
        return new IssueCollection(Items.SelectMany(i => i.Issues.AsEnumerable()));
    }
}
