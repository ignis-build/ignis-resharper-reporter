﻿namespace Ignis.ReSharper.Reporter.InspectCode.Reports.Elements;

public sealed class IssueFactory(IssueTypeCollection issueTypes)
{
    public Issue Create(string typeId, string? message, Location location)
    {
        return new Issue(message, location, issueTypes.Find(typeId));
    }
}
