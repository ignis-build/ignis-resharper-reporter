﻿using System.Xml.Linq;

namespace Ignis.ReSharper.Reporter.InspectCode.Reports.Elements;

public sealed class IssueTypeCollection(IEnumerable<IssueType> items) : ElementCollection<IssueType>(items)
{
    public static IssueTypeCollection Empty { get; } = new(Array.Empty<IssueType>());

    public static implicit operator IssueTypeCollection(IssueType[] items)
    {
        return new IssueTypeCollection(items);
    }

    public static IssueTypeCollection Get(XElement? element)
    {
        var issueTypes = element?.Element("IssueTypes");
        if (issueTypes == null) return Empty;

        return new IssueTypeCollection(IssueType.All(issueTypes));
    }

    public IssueType Find(string id)
    {
        return Items.First(x => x.Id == id)
            ;
    }
}
