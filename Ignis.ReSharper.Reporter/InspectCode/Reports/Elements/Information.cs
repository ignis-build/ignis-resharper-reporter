﻿using System.Xml.Linq;

// ReSharper disable UnusedMember.Global

namespace Ignis.ReSharper.Reporter.InspectCode.Reports.Elements;

public sealed record Information(string Solution, InspectionScope InspectionScope)
{
    public Information(InspectionScope inspectionScope) : this(string.Empty, inspectionScope)
    {
    }

    public string? Solution { get; } = Solution;
    public InspectionScope InspectionScope { get; } = InspectionScope;

    public static Information Get(XElement? element)
    {
        var information = element?.Element(nameof(Information));
        if (information == null) return new Information(string.Empty, new InspectionScope(null));

        return new Information(information.Element(nameof(Solution))?.Value ?? string.Empty,
            InspectionScope.Get(information));
    }
}
