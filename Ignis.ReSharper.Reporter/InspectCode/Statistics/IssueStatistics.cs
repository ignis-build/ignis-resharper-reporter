﻿using Ignis.ReSharper.Reporter.InspectCode.Reports.Elements;

namespace Ignis.ReSharper.Reporter.InspectCode.Statistics;

public sealed class IssueStatistics
{
    public IssueStatistics(Report report) : this(report.Issues.All())
    {
    }

    private IssueStatistics(IssueCollection issues)
    {
        Severities = new SeverityIssueCounts(issues);
    }

    public SeverityIssueCounts Severities { get; }
}
