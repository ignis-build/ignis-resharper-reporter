﻿using Nuke.Common;
using Nuke.Common.Tools.DotNet;

// ReSharper disable AllUnderscoreLocalParameterName

namespace Builds;

public interface IPush : IPack
{
    [Parameter("NuGet API Key. No API Key is used by default.")] string NugetApiKey => TryGetValue(() => NugetApiKey);

    [Parameter("Push NuGet source. Default is `nuget.org`.")]
    string NugetSource => TryGetValue(() => NugetSource) ?? "nuget.org";

    // ReSharper disable once UnusedMember.Global
    Target Push => _ => _
        .DependsOn(Pack)
        .Executes(() =>
        {
            DotNetTasks.DotNetNuGetPush(s => s
                .SetTargetPath(NuPkgDirectory / "*.nupkg")
                .SetApiKey(NugetApiKey)
                .SetSource(NugetSource));
        });
}
