﻿using Nuke.Common;
using Nuke.Common.Tools.ReSharper;
using static Nuke.Common.Tools.ReSharper.ReSharperTasks;

// ReSharper disable AllUnderscoreLocalParameterName

namespace Builds;

public interface ICleanupCode : ISolution
{
    // ReSharper disable once UnusedMember.Global
    Target CleanupCode => _ => _
        .Executes(() =>
        {
            ReSharperCleanupCode(s => s
                .SetTargetPath(Solution)
                .AddExclude("build/**/Directory.Build.*")
                .AddExclude(".nuke/**/*")
                .AddExclude("Ignis.ReSharper.Reporter.Testing/CodeQuality/*.json")
                .AddExclude("Ignis.ReSharper.Reporter.Testing/InspectCode/*.xml")
                .SetProperty("Configuration", Configuration));
        });
}
