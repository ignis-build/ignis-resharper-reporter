﻿using System.Collections.Immutable;
using System.Linq;
using Nuke.Common;
using Nuke.Common.IO;
using Nuke.Common.Tooling;
using Nuke.Common.Tools.DotNet;
using Readme;
using static Nuke.Common.Tools.DotNet.DotNetTasks;

// ReSharper disable AllUnderscoreLocalParameterName

namespace Builds;

public interface IPack : ICompile
{
    AbsolutePath NuPkgDirectory => OutputDirectory / "nupkg";

    // ReSharper disable once UnusedMember.Global
    Target Pack => _ => _
        .DependsOn(Compile)
        .Executes(() =>
        {
            var projects = new[]
                {
                    "Ignis.ReSharper.Reporter",
                    "Ignis.ReSharper.Reporter.Nuke",
                    "Ignis.ReSharper.Reporter.Tool"
                }
                .Select(project => Solution.GetProject(project))
                .ToImmutableArray();

            DotNetPack(s => s
                .SetConfiguration(Configuration)
                .SetOutputDirectory(NuPkgDirectory)
                .SetVersion(GitVersion.NuGetVersionV2)
                .SetProperty("EmbedUntrackedSources", true)
                .SetProperty("IncludeSymbols", true)
                .SetProperty("SymbolPackageFormat", "snupkg")
                .SetProperty("PackageLicenseExpression", "MIT")
                .SetAuthors("Tomo Masakura")
                .SetDescription(ReadMe.Load())
                .SetPackageProjectUrl("https://gitlab.com/ignis-build/ignis-resharper-reporter")
                .SetRepositoryUrl("https://gitlab.com/ignis-build/ignis-resharper-reporter.git")
                .SetRepositoryType("git")
                .EnableNoRestore()
                .EnableNoBuild()
                .CombineWith(projects, (s, project) => s.SetProject(project)));
        });
}
