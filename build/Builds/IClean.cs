﻿using Nuke.Common;
using Nuke.Common.IO;

// ReSharper disable AllUnderscoreLocalParameterName

namespace Builds;

public interface IClean : ISolution
{
    // ReSharper disable once UnusedMember.Global
    Target Clean => _ => _
        .Executes(() =>
        {
            OutputDirectory.CreateOrCleanDirectory();
        });
}
