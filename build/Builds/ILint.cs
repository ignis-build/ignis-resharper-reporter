﻿using Ignis.ReSharper.Reporter.InspectCode.Convert.CodeQuality;
using Ignis.ReSharper.Reporter.InspectCode.Convert.Summary;
using Ignis.ReSharper.Reporter.InspectCode.Validations.SeverityLevel;
using Ignis.ReSharper.Reporter.Nuke;
using Nuke.Common;
using Nuke.Common.Tooling;
using Nuke.Common.Tools.ReSharper;
using static Ignis.ReSharper.Reporter.Nuke.ReSharperReporterTasks;
using static Nuke.Common.Tools.ReSharper.ReSharperTasks;

// ReSharper disable AllUnderscoreLocalParameterName

namespace Builds;

public interface ILint : ICompile
{
    private string InspectCodeReportFile => OutputDirectory / "reports" / "resharper" / "inspect-code.xml";
    private string CodeQualityReportFile => OutputDirectory / "reports" / "gitlab" / "code-quality.json";

    // ReSharper disable once UnusedMember.Global
    Target Lint => _ => _
        .DependsOn(Compile)
        .Executes(() =>
        {
            ReSharperInspectCode(s => s
                .SetTargetPath(Solution)
                .SetOutput(InspectCodeReportFile)
                .AddProperty("Configuration", Configuration)
                .SetCachesHome(CacheDirectory / "resharper" / "inspect-code")
                .SetProcessArgumentConfigurator(arguments => arguments
                    .Add("--no-build")));
        });

    // ReSharper disable once UnusedMember.Global
    Target CodeQuality => _ => _
        .TriggeredBy(Lint)
        .Executes(() =>
        {
            ReSharperReport(s => s
                .SetInput(InspectCodeReportFile)
                .SetSeverity(EnsureSeverityLevel.All)
                .AddExport<CodeQualityConverter>(CodeQualityReportFile)
                .AddExport<SummaryConverter>());
        });
}
