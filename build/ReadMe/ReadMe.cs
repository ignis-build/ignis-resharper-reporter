﻿using System.IO;

namespace Readme;

static class ReadMe
{
    public static string Load()
    {
        var directory = Path.GetDirectoryName(typeof(ReadMe).Assembly.Location);
        var readme = Path.Combine(directory!, "ReadMe", "readme.txt");
        return File.ReadAllText(readme);
    }
}
