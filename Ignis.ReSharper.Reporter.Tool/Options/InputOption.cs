﻿using System.CommandLine;

namespace Ignis.ReSharper.Reporter.Tool.Options;

internal sealed class InputOption : ToolOption<FileInfo>
{
    private InputOption() : base(new Option<FileInfo>("--input")
    {
        IsRequired = true,
        Description = "Input InspectCode report file."
    })
    {
    }

    public static InputOption Instance { get; } = new();
}
