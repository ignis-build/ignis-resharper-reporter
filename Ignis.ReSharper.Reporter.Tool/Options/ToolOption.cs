﻿using System.CommandLine;
using System.CommandLine.Invocation;

namespace Ignis.ReSharper.Reporter.Tool.Options;

internal class ToolOption<T>
{
    private readonly Option<T> _option;

    protected ToolOption(Option<T> option)
    {
        _option = option;
    }

    public void AddTo(Command command)
    {
        command.AddOption(_option);
    }

    public T GetFrom(InvocationContext context)
    {
        var value = TryGetFrom(context);
        return value ?? throw new InvalidOperationException();
    }

    // ReSharper disable once ReturnTypeCanBeNotNullable
    public T? TryGetFrom(InvocationContext context)
    {
        return context.ParseResult.GetValueForOption(_option)!;
    }
}
