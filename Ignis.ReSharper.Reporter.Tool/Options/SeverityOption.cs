﻿using System.CommandLine;
using System.CommandLine.Parsing;
using Ignis.ReSharper.Reporter.InspectCode.Validations.SeverityLevel;

namespace Ignis.ReSharper.Reporter.Tool.Options;

internal sealed class SeverityOption : ToolOption<EnsureSeverityLevel>
{
    private SeverityOption() : base(Create())
    {
    }

    public static SeverityOption Instance { get; } = new();

    private static Option<EnsureSeverityLevel> Create()
    {
        return new Option<EnsureSeverityLevel>("--severity", ParseArgument, true)
        {
            Description =
                $"Ensure that there are no issues above the specified severity. ({EnsureSeverityLevel.AllItems})"
        };

        EnsureSeverityLevel ParseArgument(ArgumentResult result)
        {
            if (!result.Tokens.Any()) return EnsureSeverityLevel.Default;

            var value = result.Tokens[0].Value;
            return EnsureSeverityLevel.Parse(value);
        }
    }
}
