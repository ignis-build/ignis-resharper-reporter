﻿using System.Collections.Immutable;
using System.CommandLine;
using System.CommandLine.Parsing;
using Ignis.ReSharper.Reporter.InspectCode;

namespace Ignis.ReSharper.Reporter.Tool.Options;

internal sealed class ExportOption : ToolOption<ImmutableArray<ExportReport>>
{
    private ExportOption() : base(Create())
    {
    }

    public static ExportOption Instance { get; } = new();

    private static Option<ImmutableArray<ExportReport>> Create()
    {
        return new Option<ImmutableArray<ExportReport>>("--export", ParseArgument)
        {
            AllowMultipleArgumentsPerToken = true,
            IsRequired = true,
            Description = "Report export definition. (ex: `type=codequality;file=codequality.json`)"
        };

        ImmutableArray<ExportReport> ParseArgument(ArgumentResult result)
        {
            return ExportReport.Parse(result.Tokens.Select(token => token.Value))
                .ToImmutableArray();
        }
    }
}
