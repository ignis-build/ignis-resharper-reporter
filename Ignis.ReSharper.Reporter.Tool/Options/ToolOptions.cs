﻿using System.CommandLine;

namespace Ignis.ReSharper.Reporter.Tool.Options;

internal sealed class ToolOptions
{
    private ToolOptions()
    {
    }

    public static ToolOptions Instance { get; } = new();

    public void AddTo(Command command)
    {
        Input.AddTo(command);
        Export.AddTo(command);
        Severity.AddTo(command);
    }

#pragma warning disable CA1822
    // ReSharper disable MemberCanBeMadeStatic.Global
    public InputOption Input => InputOption.Instance;
    public ExportOption Export => ExportOption.Instance;
    public SeverityOption Severity => SeverityOption.Instance;
    // ReSharper restore MemberCanBeMadeStatic.Global
#pragma warning restore CA1822
}
