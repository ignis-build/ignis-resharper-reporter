﻿using System.CommandLine;
using Ignis.ReSharper.Reporter.Logging;

namespace Ignis.ReSharper.Reporter.Tool;

internal sealed class ConsoleLogger(IConsole console) : Logger
{
    public override void WriteLine(string message)
    {
        console.WriteLine(message);
    }
}
