﻿using System.CommandLine.IO;
using System.IO;

namespace Ignis.ReSharper.Reporter.Tool.Helper.Console.Recording;

internal sealed class RecordingStandardStreamWriter(Stream stream) : IStandardStreamWriter
{
    private readonly StreamWriter _writer = new(stream, leaveOpen: true);

    public void Write(string value)
    {
        _writer.Write(value);
        _writer.Flush();
    }
}
