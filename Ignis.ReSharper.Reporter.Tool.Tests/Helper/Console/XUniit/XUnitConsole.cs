﻿using System.CommandLine;
using System.CommandLine.IO;
using Xunit.Abstractions;

namespace Ignis.ReSharper.Reporter.Tool.Helper.Console.XUniit;

internal sealed class XUnitConsole(ITestOutputHelper output) : IConsole
{
    public IStandardStreamWriter Out { get; } = new XUnitStreamWriter(output);
    public IStandardStreamWriter Error { get; } = new XUnitStreamWriter(output);
    public bool IsOutputRedirected => false;
    public bool IsErrorRedirected => false;
    public bool IsInputRedirected => false;
}
