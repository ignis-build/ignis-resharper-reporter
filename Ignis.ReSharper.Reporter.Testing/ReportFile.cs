﻿namespace Ignis.ReSharper.Reporter.Testing;

public sealed class ReportFile(string path)
{
    private readonly Lazy<Text> _lazy = new(() => new Text(File.ReadAllText(path)));
    private readonly string _path = path;

    public Text Text => _lazy.Value;

    public void ForceDelete()
    {
        if (File.Exists(_path)) File.Delete(_path);
    }

    public static implicit operator string(ReportFile file)
    {
        return file._path;
    }

    public override string ToString()
    {
        return _path;
    }
}
