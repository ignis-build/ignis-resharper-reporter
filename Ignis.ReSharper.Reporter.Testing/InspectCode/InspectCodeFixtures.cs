﻿namespace Ignis.ReSharper.Reporter.Testing.InspectCode;

public sealed class InspectCodeFixtures
{
    public ReportFile Solution { get; } = File("solution.xml");
    public ReportFile Project { get; } = File("project.xml");
    public ReportFile NoIssues { get; } = File("no-issues.xml");
    public ReportFile SingleIssue { get; } = File("single-issue.xml");

    private static ReportFile File(string file)
    {
        return new ReportFile(Path.Combine("InspectCode", file));
    }
}
