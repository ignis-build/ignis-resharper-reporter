﻿using System.Collections.Generic;
using System.Text;
using Ignis.ReSharper.Reporter.Testing;
using PowerAssert;
using Xunit;

namespace Ignis.ReSharper.Reporter.IO;

public sealed class DirectoryTest
{
    private readonly MockFileSystem _fileSystem = new MockFileSystem()
        .AddDirectory("/")
        .AddDirectory("/foo");

    [Fact]
    public void TestMkdirpAbsolutePath()
    {
        IO.Directory.FromFile("/foo/bar/buzz/sample.txt", _fileSystem)
            .Mkdirp();

        Text actual = _fileSystem.ToString();
        Text expected = @"
mkdir /foo/bar
mkdir /foo/bar/buzz
";

        PAssert.IsTrue(() => actual == expected);
    }

    [Fact]
    public void TestMkdirpRelativePath()
    {
        IO.Directory.FromFile("./sample.txt", _fileSystem)
            .Mkdirp();

        var actual = _fileSystem.ToString();

        PAssert.IsTrue(() => actual == string.Empty);
    }

    [Fact]
    public void TestMkdirpFilenameOnly()
    {
        IO.Directory.FromFile("sample.txt", _fileSystem)
            .Mkdirp();

        var actual = _fileSystem.ToString();

        PAssert.IsTrue(() => actual == string.Empty);
    }

    [Fact]
    public void TestExists()
    {
        PAssert.IsTrue(() => Directory("/foo").Exists);
    }

    [Fact]
    public void TestExistsCurrentDirectory()
    {
        PAssert.IsTrue(() => Directory(".").Exists);
    }

    [Fact]
    public void TestExistsParentDirectory()
    {
        PAssert.IsTrue(() => Directory("..").Exists);
    }

    [Fact]
    public void TestNotExists()
    {
        PAssert.IsTrue(() => !Directory("/bar").Exists);
    }

    private Directory Directory(string directory)
    {
        return new Directory(directory, _fileSystem);
    }

    private sealed class MockFileSystem : FileSystem
    {
        private readonly HashSet<string> _directories = new();
        private readonly StringBuilder _log = new();

        public MockFileSystem AddDirectory(string directory)
        {
            _directories.Add(directory);
            return this;
        }

        public override void Mkdir(string directory)
        {
            _log.AppendLine($"mkdir {Normalize(directory)}");
        }

        public override bool ExistsDirectory(string directory)
        {
            return _directories.Contains(Normalize(directory));
        }

        public override string ToString()
        {
            return _log.ToString().Trim();
        }

        private static string Normalize(string directory)
        {
            return directory.Replace("\\", "/");
        }
    }
}
