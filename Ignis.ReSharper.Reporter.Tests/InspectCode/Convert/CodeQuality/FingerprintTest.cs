﻿using PowerAssert;
using Xunit;

namespace Ignis.ReSharper.Reporter.InspectCode.Convert.CodeQuality;

public sealed class FingerprintTest
{
    [Fact]
    public void TestFingerprint()
    {
        var target = new Fingerprint(new CodeQualityIssue("description", CodeQualitySeverity.Info, "Sample.cs", 12));

        PAssert.IsTrue(() => target.ToString() == "0b91a198995f11e1c078550149c38c6004ee352f");
    }
}
