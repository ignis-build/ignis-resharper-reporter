﻿using System;
using Ignis.ReSharper.Reporter.Testing;
using PowerAssert;
using Xunit;

namespace Ignis.ReSharper.Reporter.InspectCode.Convert.Summary;

public sealed class SummaryConverterTest : IDisposable
{
    private readonly MemoryOutput _output = new();
    private readonly InspectCodeReport _report = InspectCodeReport.Load(Fixtures.InspectCode.Solution);
    private readonly ReportConverterType _target = ReportConverterType.Parse("type=summary");

    public void Dispose()
    {
        _output.Dispose();
    }

    [Fact]
    public void TestConvert()
    {
        _report.Export(new ExportReport(_target, _output));

        Text actual = _output.ToString();
        Text expected = @"
Severity    Count
-----------------
INFO            0
HINT            0
SUGGESTION      7
WARNING         5
ERROR           0
";

        PAssert.IsTrue(() => actual == expected);
    }
}
