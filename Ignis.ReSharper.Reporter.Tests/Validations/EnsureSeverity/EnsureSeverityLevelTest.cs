﻿using System;
using System.Collections.Generic;
using Ignis.ReSharper.Reporter.InspectCode.Reports.Elements;
using Ignis.ReSharper.Reporter.InspectCode.Validations.SeverityLevel;
using Ignis.ReSharper.Reporter.Testing;
using PowerAssert;
using Xunit;
using Xunit.Abstractions;

namespace Ignis.ReSharper.Reporter.Validations.EnsureSeverity;

public sealed class EnsureSeverityLevelTest(ITestOutputHelper output)
{
    [Theory]
    [MemberData(nameof(ParseSource))]
    public void TestParse(string severity, EnsureSeverityLevel expected)
    {
        var actual = EnsureSeverityLevel.Parse(severity);

        output.WriteLine(actual.GetHashCode().ToString());
        output.WriteLine(expected.GetHashCode().ToString());
        PAssert.IsTrue(() => actual == expected);
    }

    [Fact]
    public void TestEnsureNone()
    {
        var report = Report.Load(Fixtures.InspectCode.Solution);

        // do not thrown exception.
        report.EnsureNoIssues(EnsureSeverityLevel.None);
    }

    [Fact]
    public void TestEnsureWarning()
    {
        var report = Report.Load(Fixtures.InspectCode.Solution);

        PAssert.Throws<InvalidOperationException>(() => report.EnsureNoIssues(Severity.Warning));
    }

    [Fact]
    public void TestEnsureError()
    {
        var report = Report.Load(Fixtures.InspectCode.Solution);

        // do not thrown exception.
        report.EnsureNoIssues(Severity.Error);
    }

    [Fact]
    public void TestEnsureAll()
    {
        var report = Report.Load(Fixtures.InspectCode.Solution);

        PAssert.Throws<InvalidOperationException>(() => report.EnsureNoIssues(EnsureSeverityLevel.All));
    }

    public static IEnumerable<object[]> ParseSource()
    {
        yield return ["all", EnsureSeverityLevel.All];
        yield return ["ALL", EnsureSeverityLevel.All];
        yield return ["All", EnsureSeverityLevel.All];
        yield return ["Error", Severity.Error];
        yield return ["Warning", Severity.Warning];
        yield return ["Suggestion", Severity.Suggestion];
        yield return ["Hint", Severity.Hint];
        yield return ["Info", Severity.Info];
    }
}
